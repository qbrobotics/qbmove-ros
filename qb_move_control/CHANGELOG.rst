^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package qb_move_control
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.0.2 (2023-04-26)
------------------
* FEAT: added parameters to specify the serial port to connect to.

3.0.1 (2023-03-23)
------------------
* modified qbsoftclaw_waypoints.yaml in order to explain the deflection controller
* Bug fixed when a simple qbmove is controlled (with no deflection command)
* Implemented double controller.

3.0.0 (2022-07-06)
------------------

2.2.1 (2021-08-27)
------------------

2.2.0 (2021-08-26)
------------------
* Add Gazebo SoftHand and SoftHand 2 Motors plugin skeletons
* Set a better name for fake joint simulation variables
* Fix cmake resource installation
* Added limits settings in qbmove. Fixed some minor bugs
* Fixed stiffness misreading in qbmove with new firmware version.
* Fix qb SoftClaw launch
* Add Gazebo plugin for qbmove devices with qbMoveHWSim
* Add joint limits for qb SoftClaw
* Hide motor_positions_trajectory_controller params
* Added some images to exapli the software functionalities. Added waypoints. Fixed a bug.
* Revert qb SoftClaw launch file elimination
* Fixed a bug(position factor in constructor was not considered). Basic claw plugin implemented and tested.
* Update control launch file
* modified .xacro files in order to change the URDF claw origin. Added a new launch for controlling the SoftClaw.
* Add qb SoftClaw meshes and its URDF

2.1.3 (2019-10-07)
------------------

2.1.2 (2019-06-11)
------------------

2.1.1 (2019-06-11)
------------------

2.1.0 (2019-05-28)
------------------
* Add an interactive marker to control the device
* Fix minor style issues

2.0.2 (2018-08-09)
------------------

2.0.1 (2018-08-07)
------------------
* Set safer default values

2.0.0 (2018-05-30)
------------------
* Update README
* Refactor launch files
* Remove control node
* Add blocking setCommands method support
* Refactor launch files
* Fix destructor calls on ROS shutdown

1.0.6 (2017-11-24)
------------------

1.0.5 (2017-06-27)
------------------
* Fix C++11 support for cmake version less than 3.1

1.0.4 (2017-06-23)
------------------
* Update cmake version to match Kinetic standards

1.0.3 (2017-06-22)
------------------
* fix dependencies

1.0.2 (2017-06-21)
------------------
* fix cmake settings to solve isolated builds

1.0.1 (2017-06-19)
------------------
* first public release for Kinetic
