^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package qb_move_description
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.0.2 (2023-04-26)
------------------

3.0.1 (2023-03-23)
------------------
* FEAT: Changed ee link orientation in URDFs
* FEAT: Changed end-effector link position for qb SoftClaw.
* Implemented double controller.

3.0.0 (2022-07-06)
------------------
* Modified .xacro files in order to fit new xacro package version. This commint is suitable with xacro version 1.13.15 -- see https://github.com/ros/xacro/commit/7b5c20907375400cf4db6993a13238e86b38acf3 for more details.
* Fixed errors on URDFs files

2.2.1 (2021-08-27)
------------------

2.2.0 (2021-08-26)
------------------
* Fix qbmove flanges inertial values
* Fix cmake resource installation
* Add Gazebo plugin for qbmove devices with qbMoveHWSim
* Fixed a bug(position factor in constructor was not considered). Basic claw plugin implemented and tested.
* modified .xacro files in order to change the URDF claw origin. Added a new launch for controlling the SoftClaw.
* Fix finger down pad URDF
* Fix URDF macros to set a custom parent link name
* Add qb SoftClaw meshes and its URDF

2.1.3 (2019-10-07)
------------------
* Fix initial command and velocity limit for qbmoves

2.1.2 (2019-06-11)
------------------

2.1.1 (2019-06-11)
------------------

2.1.0 (2019-05-28)
------------------
* Fix description values
* Fix utilities to build delta URDF easily

2.0.2 (2018-08-09)
------------------

2.0.1 (2018-08-07)
------------------

2.0.0 (2018-05-30)
------------------
* Update xacro models
* Refactor launch files
* Add flange meshes and few basic configurations
* Add qbmove meshes
* Refactor launch files

1.0.6 (2017-11-24)
------------------

1.0.5 (2017-06-27)
------------------

1.0.4 (2017-06-23)
------------------
* Update cmake version to match Kinetic standards

1.0.3 (2017-06-22)
------------------

1.0.2 (2017-06-21)
------------------
* fix cmake settings to solve isolated builds

1.0.1 (2017-06-19)
------------------
* first public release for Kinetic
