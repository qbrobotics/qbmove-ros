/***
 *  Software License Agreement: BSD 3-Clause License
 *
 *  Copyright (c) 2016-2018, qbrobotics®
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 *  following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 *  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 *  USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <qb_move_gazebo/qb_move_gazebo_hardware_interface.h>

using namespace qb_move_gazebo_hardware_interface;

void qbMoveHWSim::computeDeflections() {
  // in every case clamp to 0.8 radians == ~45 degrees
  if (joints_.commands.at(0) != joint_commands_old_.at(0) || joints_.commands.at(1) != joint_commands_old_.at(1)) {  // motor position commands are used
    deflection_references_.at(0) = clamp(joints_.positions.at(2)-joints_.commands.at(0), 0.8);
    deflection_references_.at(1) = clamp(joints_.positions.at(2)-joints_.commands.at(1), 0.8);
  } else {  // shaft position and stiffness commands are used
    deflection_references_.at(0) = clamp(joints_.positions.at(2)-(joints_.commands.at(2)+joints_.commands.at(3)), 0.8);
    deflection_references_.at(1) = clamp(joints_.positions.at(2)-(joints_.commands.at(2)-joints_.commands.at(3)), 0.8);
  }

  // update old joint commands
  for (int i=0; i<joints_.commands.size(); i++) {
    joint_commands_old_.at(i) = joints_.commands.at(i);
  }
}

double qbMoveHWSim::computeElasticEnergy() {
  return QBMOVE_PARAM_K1*(std::cosh(QBMOVE_PARAM_A1*deflection_references_.at(0))-1)/QBMOVE_PARAM_A1
       + QBMOVE_PARAM_K2*(std::cosh(QBMOVE_PARAM_A2*deflection_references_.at(1))-1)/QBMOVE_PARAM_A1;
}

double qbMoveHWSim::computeOutputStiffness() {
  return QBMOVE_PARAM_A1*QBMOVE_PARAM_K1*std::cosh(QBMOVE_PARAM_A1*deflection_references_.at(0))
       + QBMOVE_PARAM_A2*QBMOVE_PARAM_K2*std::cosh(QBMOVE_PARAM_A2*deflection_references_.at(1));
}

double qbMoveHWSim::computeOutputTorque() {
  return QBMOVE_PARAM_K1*std::sinh(QBMOVE_PARAM_A1*deflection_references_.at(0))
       + QBMOVE_PARAM_K2*std::sinh(QBMOVE_PARAM_A2*deflection_references_.at(1));
}

bool qbMoveHWSim::initSim(const std::string &robot_namespace, ros::NodeHandle model_nh, gazebo::physics::ModelPtr parent_model, const urdf::Model *const urdf_model, std::vector<transmission_interface::TransmissionInfo> transmissions) {
  model_nh_ = ros::NodeHandle(robot_namespace);
  urdf_model_ = *urdf_model;

  std::vector<std::string> joint_names;
  for (auto const &transmission : transmissions) {
    if (!startsWith(transmission.name_, trailNamespace(model_nh.getNamespace()))) {
      continue;  // select only joints of the specific qbmove
    }

    ROS_INFO_STREAM_NAMED("qb_move_gazebo_hardware_interface","Initializing qbMoveHWSim of '" << model_nh.getNamespace() << "'...");
    for (auto const &joint : transmission.joints_) {
      gazebo::physics::JointPtr sim_joint = parent_model->GetJoint(joint.name_);
      if (!sim_joint) {
        ROS_ERROR_STREAM_NAMED("qb_move_gazebo_hardware_interface","This robot has a joint named '" << joint.name_ << "' which is not in the gazebo model.");
        return false;
      }
      sim_joints_.push_back(sim_joint);
      joint_names.push_back(joint.name_);
      ROS_INFO_STREAM_NAMED("qb_move_gazebo_hardware_interface"," * Added joint '" << joint.name_ << "'.");
    }
  }
  joints_.setJoints(joint_names);

  //TODO: add a check on the number on transmission and check if multiple qbmoves can be spawned
  if (joints_.names.size() != 4) {
    ROS_ERROR_STREAM_NAMED("qb_move_gazebo_hardware_interface","Wrong number of joints [" << joints_.names.size() << "]");
    return false;
  }

  joint_commands_old_.resize(joint_names.size());
  deflection_references_.resize(2);
  interfaces_.initialize(this, joints_);
  joint_limits_.initialize(model_nh_, joints_, urdf_model_, interfaces_.joint_position);
  return true;
}

void qbMoveHWSim::readSim(ros::Time time, ros::Duration period) {
  for (int i=0; i<sim_joints_.size(); i++) {
#if GAZEBO_MAJOR_VERSION >= 8
    double position = sim_joints_.at(i)->Position(0);
#else
    double position = sim_joints_.at(i)->GetAngle(0).Radian();
#endif
    // read joints data from Gazebo
    joints_.positions.at(i) += angles::shortest_angular_distance(joints_.positions.at(i), position);
    joints_.velocities.at(i) = sim_joints_.at(i)->GetVelocity(0);
    joints_.efforts.at(i) = sim_joints_.at(i)->GetForce(0);
  }
}

void qbMoveHWSim::updateShaftPositionLimits() {
  // the shaft limits [radians] depend on fixed motor limits [radians] and variable stiffness preset [0,1]
  joints_.limits.at(2).max_position = std::min(joints_.limits.at(2).max_position, QBMOVE_MAX_MOTOR_LIMIT - std::abs(joints_.commands.at(3)*QBMOVE_STIFF_PERCENT_TO_RAD));
  joints_.limits.at(2).min_position = std::max(joints_.limits.at(2).min_position, QBMOVE_MIN_MOTOR_LIMIT + std::abs(joints_.commands.at(3)*QBMOVE_STIFF_PERCENT_TO_RAD));
}

void qbMoveHWSim::writeSim(ros::Time time, ros::Duration period) {
  // the variable stiffness decreases the shaft position limits (all the other limits are fixed)
  updateShaftPositionLimits();

  // enforce joint limits for all registered interfaces
  joint_limits_.enforceLimits(period);

  // compute deflection references w.r.t. the controller mode which is currently enabled (ROS does not allow to do it properly, check for command changes)
  computeDeflections();

  // send joint commands to Gazebo
  sim_joints_.at(2)->SetForce(0, -computeOutputTorque());
  //sim_joints_.at(2)->SetStiffness(0, computeOutputStiffness());
}

PLUGINLIB_EXPORT_CLASS(qb_move_gazebo_hardware_interface::qbMoveHWSim, gazebo_ros_control::RobotHWSim);