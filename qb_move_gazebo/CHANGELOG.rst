^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package qb_move_gazebo
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.0.2 (2023-04-26)
------------------

3.0.1 (2023-03-23)
------------------

3.0.0 (2022-07-06)
------------------
* Set c++ standard 17 in gazebo packages

2.2.1 (2021-08-27)
------------------

2.2.0 (2021-08-26)
------------------
* Added changelog file in gazebo package.
* Refactor Gazebo plugin to prepare for SoftHand simulation
* Add Gazebo plugin for qbmove devices with qbMoveHWSim

