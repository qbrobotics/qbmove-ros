/***
 *  Software License Agreement: BSD 3-Clause License
 *
 *  Copyright (c) 2016-2021, qbrobotics®
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 *  following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 *  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 *  USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <qb_move_hardware_interface/qb_move_hardware_interface.h>

using namespace qb_move_hardware_interface;

qbMoveHW::qbMoveHW()
    : qbDeviceHW(std::make_shared<qb_move_transmission_interface::qbMoveTransmission>(), {"motor_1_joint", "motor_2_joint", "shaft_joint"}, {"motor_1_joint", "motor_2_joint", "shaft_joint", "stiffness_preset_virtual_joint"}) {
      ROS_INFO_STREAM("initializing qbmove...");
}

qbMoveHW::qbMoveHW(const std::vector<double> &position_factor)
    : qbDeviceHW(std::make_shared<qb_move_transmission_interface::qbMoveTransmission>(false, position_factor, 1./3000, 0.2, 0.001), {"motor_1_joint", "motor_2_joint", "shaft_joint"}, {"motor_1_joint", "motor_2_joint", "shaft_joint", "stiffness_preset_virtual_joint", "deflection_virtual_joint"}) {
      ROS_INFO_STREAM("initializing qb SoftClaw...");
}

qbMoveHW::~qbMoveHW() {

}

std::vector<std::string> qbMoveHW::getJoints() {
  if (command_with_position_and_preset_) {
    return {joints_.names.at(2), joints_.names.at(3)};
  }
  return {joints_.names.at(0), joints_.names.at(1)};
}

int qbMoveHW::getMaxStiffness() {
  std::smatch match_row, match_value;
  std::string device_info(getInfo());
  if (std::regex_search(device_info, match_row, std::regex("Param <Ctrl, Inpt, Stup, Limt, Stif>: (.*)")) || std::regex_search(device_info, match_row, std::regex("Max stiffness: [[:digit:]]+"))) {
    std::string max_stiffness(match_row[0]);
    std::regex_search(max_stiffness, match_value, std::regex("[[:digit:]]+"));
    ROS_INFO_STREAM("Device stiffness: " << std::stoi(match_value[0]));
    return std::stoi(match_value[0]);
  }
  return 3000;  // default limit of stiffness value
}

bool qbMoveHW::init(ros::NodeHandle &root_nh, ros::NodeHandle &robot_hw_nh) {
  if (!qb_device_hardware_interface::qbDeviceHW::init(root_nh, robot_hw_nh)) {
    return false;
  }

  std::vector<double> commands;
  if (getCommands(commands) == -1) {
    ROS_ERROR_STREAM_THROTTLE_NAMED(60 ,"qbmove_hw", "[qbmove_hw] cannot retrieve command references from device [" << device_.id << "].");
    return false;
  }
  joints_.commands.at(0) = commands.at(0);
  joints_.commands.at(1) = commands.at(1);
  joints_.commands.at(2) = (commands.at(0) + commands.at(1)) / 2;
  joints_.commands.at(3) = std::abs(commands.at(0) - commands.at(1)) / 2;


  std::string default_controller;
  if (!robot_hw_nh.getParam("default_controller", default_controller)) {
    ROS_ERROR_STREAM_THROTTLE_NAMED(60 ,"qbmove_hw", "[qbmove_hw] cannot retrieve 'default_controller' from the Parameter Server [" << robot_hw_nh.getNamespace() << "].");
    return false;
  }
  std::vector<std::string> default_controller_joints;
  if (!robot_hw_nh.getParam(ros::names::parentNamespace(robot_hw_nh.getNamespace()) + default_controller + "/joints", default_controller_joints)) {
    ROS_ERROR_STREAM_THROTTLE_NAMED(60 ,"qbmove_hw", "[qbmove_hw] cannot retrieve 'joints' from the controller in the Parameter Server [" << ros::names::parentNamespace(robot_hw_nh.getNamespace()) + default_controller  << "].");
    return false;
  }

  trajectory_msgs::JointTrajectoryPoint point;
  point.positions.resize(commands.size());
  point.velocities.resize(commands.size());
  point.accelerations.resize(commands.size());
  point.effort.resize(commands.size());
  point.positions.at(0) = joints_.commands.at(2);
  point.positions.at(1) = joints_.commands.at(3);
  
 
  point.time_from_start = ros::Duration(0.1);
  controller_first_point_trajectory_.points.push_back(point);
  controller_first_point_trajectory_.joint_names = default_controller_joints;
  // cannot publish it yet (the controller has not been spawned yet) first_command_publisher_.publish(first_point_trajectory_);

  // for qbmoves is important that the following assertions hold
  ROS_ASSERT(device_.encoder_resolutions.at(0) == device_.encoder_resolutions.at(1));
  ROS_ASSERT(device_.position_limits.size() == 4);
  ROS_ASSERT(device_.position_limits.at(0) == device_.position_limits.at(2) && device_.position_limits.at(1) == device_.position_limits.at(3));

  // if the device interface initialization has succeed the device info have been retrieved
  std::static_pointer_cast<qb_move_transmission_interface::qbMoveTransmission>(transmission_.getTransmission())->setPositionFactor(device_.encoder_resolutions);
  std::static_pointer_cast<qb_move_transmission_interface::qbMoveTransmission>(transmission_.getTransmission())->setPresetFactor(getMaxStiffness());
  command_with_position_and_preset_ = std::static_pointer_cast<qb_move_transmission_interface::qbMoveTransmission>(transmission_.getTransmission())->getCommandWithPoistionAndPreset();
  preset_percent_to_radians_ = std::static_pointer_cast<qb_move_transmission_interface::qbMoveTransmission>(transmission_.getTransmission())->getPresetPercentToRadians();
  position_ticks_to_radians_ = std::static_pointer_cast<qb_move_transmission_interface::qbMoveTransmission>(transmission_.getTransmission())->getPositionFactor().front();
  if(device_.use_joint_limits){
    max_motor_limits_ = joints_.limits.at(2).max_position;
    min_motor_limits_ = joints_.limits.at(2).min_position;
  } else {
    max_motor_limits_ = device_.position_limits.at(1)*position_ticks_to_radians_;
    min_motor_limits_ = device_.position_limits.at(0)*position_ticks_to_radians_;
  }

  use_interactive_markers_ = root_nh.param<bool>("use_interactive_markers", false) && !root_nh.param<bool>("use_waypoints", false) && command_with_position_and_preset_;
  if (use_interactive_markers_) {
    interactive_interface_.initMarkers(root_nh, getDeviceNamespace(), getJoints());
  }

  return true;
}

void qbMoveHW::read(const ros::Time &time, const ros::Duration &period) {
  // read actuator state from the hardware (convert to proper measurement units)
  qb_device_hardware_interface::qbDeviceHW::read(time, period);

  if (use_interactive_markers_) {
    interactive_interface_.setMarkerState({joints_.positions.at(2), joints_.positions.at(3)});
  }
}

void qbMoveHW::updateShaftPositionLimits() {
  // the shaft limits [radians] depend on fixed motor limits [radians] and variable stiffness preset [0,1]
  if(joints_.positions.at(2) > 2 || joints_.positions.at(2) < -2) { // shaft near factory limits -> restict device limits according to its stiffness
    joints_.limits.at(2).max_position = max_motor_limits_ - std::abs(joints_.commands.at(3)*preset_percent_to_radians_);
    joints_.limits.at(2).min_position = min_motor_limits_ + std::abs(joints_.commands.at(3)*preset_percent_to_radians_);
  } else {
    joints_.limits.at(2).max_position = max_motor_limits_;
    joints_.limits.at(2).min_position = min_motor_limits_;
  }
}

void qbMoveHW::write(const ros::Time &time, const ros::Duration &period) {
  // the variable stiffness decreases the shaft position limits (all the other limits are fixed)
  updateShaftPositionLimits();

  // send actuator command to the hardware (saturate and convert to proper measurement units)
  qb_device_hardware_interface::qbDeviceHW::write(time, period);
}

PLUGINLIB_EXPORT_CLASS(qb_move_hardware_interface::qbMoveHW, hardware_interface::RobotHW)