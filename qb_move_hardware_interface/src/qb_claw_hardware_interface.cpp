/***
 *  Software License Agreement: BSD 3-Clause License
 *
 *  Copyright (c) 2016-2020, qbrobotics®
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 *  following conditions are met:
 *
 *  * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *
 *  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 *  * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 *  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 *  USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <qb_move_hardware_interface/qb_claw_hardware_interface.h>

using namespace qb_claw_hardware_interface;

qbClawHW::qbClawHW() 
    : qb_move_hardware_interface::qbMoveHW(std::vector<double>(3, 1./4200)) {
      initializeSwitchControlModeService();
    }

qbClawHW::~qbClawHW() {}

std::vector<std::string> qbClawHW::getJoints() {
  if (std::static_pointer_cast<qb_move_transmission_interface::qbMoveTransmission>(transmission_.getTransmission())->getCommandWithPoistionAndPreset()) {
    return {joints_.names.at(2), joints_.names.at(3)};
  } else if(std::static_pointer_cast<qb_move_transmission_interface::qbMoveTransmission>(transmission_.getTransmission())->getCommandWithDeflection()) {
    return {joints_.names.at(4)};
  } else {
    return {joints_.names.at(0), joints_.names.at(1)};
  }
  
}

void qbClawHW::initializeSwitchControlModeService(){
    switch_control_mode_srv_ = node_handle_.serviceClient<qb_device_srvs::SetControlMode>("/communication_handler/set_control_mode", true);
}

void qbClawHW::doSwitch(const std::list<hardware_interface::ControllerInfo> &start_list, const std::list<hardware_interface::ControllerInfo> &stop_list){
  ROS_INFO_STREAM_NAMED("qbclaw_hw", "[qbclaw_hw] is executing the switch of the controllers.");
  qb_device_srvs::SetControlMode srv;
  srv.request.id = device_.id;
  srv.request.max_repeats = device_.max_repeats;
  srv.request.control = "deflection"; // set the deflection control mode
  bool deflection_controller_stopped = false;
  bool position_and_stiff_controller_started = false;

  std::for_each(stop_list.begin(), stop_list.end(), [&](hardware_interface::ControllerInfo controller){
    ROS_INFO_STREAM_NAMED("qbclaw_hw", "[qbclaw_hw] " << controller.name << " stopped");
    if(controller.name.find("deflection") != std::string::npos) {
      srv.request.control = "position"; // set the position control mode
      deflection_controller_stopped = true;
    } 
  });

  std::for_each(start_list.begin(), start_list.end(), [&](hardware_interface::ControllerInfo controller){
    ROS_INFO_STREAM_NAMED("qbclaw_hw", "[qbclaw_hw] " << controller.name << " started");
    if(controller.name.find("position_and_preset") != std::string::npos) {
      position_and_stiff_controller_started = true;
    } 
  });

  if(deflection_controller_stopped && position_and_stiff_controller_started) {
    std::static_pointer_cast<qb_move_transmission_interface::qbMoveTransmission>(transmission_.getTransmission())->switchFromDeflectionToPosStiff(true);
  } else {
    std::static_pointer_cast<qb_move_transmission_interface::qbMoveTransmission>(transmission_.getTransmission())->switchFromDeflectionToPosStiff(false);
  }
  deactivateMotors();
  switch_control_mode_srv_.call(srv);
  if(srv.response.success) {
    activateMotors();
  } else {
    ROS_ERROR_STREAM_NAMED("qbclaw_hw", "[qbclaw_hw] Error while setting the " << srv.request.control << " control mode!");
  }
  
}

PLUGINLIB_EXPORT_CLASS(qb_claw_hardware_interface::qbClawHW, hardware_interface::RobotHW)