/***
 *  Software License Agreement: BSD 3-Clause License
 *  
 *  Copyright (c) 2016-2020, qbrobotics®
 *  All rights reserved.
 *  
 *  Redistribution and use in source and binary forms, with or without modification, are permitted provided that the
 *  following conditions are met:
 *  
 *  * Redistributions of source code must retain the above copyright notice, this list of conditions and the
 *    following disclaimer.
 *  
 *  * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the
 *    following disclaimer in the documentation and/or other materials provided with the distribution.
 *  
 *  * Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote
 *    products derived from this software without specific prior written permission.
 *  
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 *  INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 *  USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef QB_CLAW_HARDWARE_INTERFACE_H
#define QB_CLAW_HARDWARE_INTERFACE_H

#include <qb_move_hardware_interface/qb_move_hardware_interface.h>

namespace qb_claw_hardware_interface {

  class qbClawHW : public qb_move_hardware_interface::qbMoveHW {
    public:
      qbClawHW();

      ~qbClawHW() override;

      /**
       * @brief Return the shaft position and stiffness preset joints whether \p command_with_position_and_preset_ is \p true; 
       * the deflection joint whether \p command_with_deflection_ is \p true; the
       * motors joints otherwise.
       * \return The vector of controller joint names.
       */
      std::vector<std::string> getJoints() override;

      /**
       * @brief Initialize the control mode srv for qb SoftClaw (allows to switch from position to deflection)
       *
       */
      void initializeSwitchControlModeService();

      /**
       * @brief Method called when a controller switch is executed
       * 
       * @param start_list list of controllers to start
       * @param stop_list list of controllers to stop
       */
      void doSwitch(const std::list<hardware_interface::ControllerInfo> &start_list, const std::list<hardware_interface::ControllerInfo> &stop_list);

    private:
      ros::ServiceClient switch_control_mode_srv_;
  }; // class qbClawHW

} // namespace qb_move_hardware_interface

#endif // QB_CLAW_TRANSMISSION_H