^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package qb_move_hardware_interface
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.0.2 (2023-04-26)
------------------
* FEAT: fix startup behaviour

3.0.1 (2023-03-23)
------------------
* Added effort and velocity info also for deflection control
* Bug fixed when a simple qbmove is controlled (with no deflection command)
* Implemented double controller.

3.0.0 (2022-07-06)
------------------
* removed some debug std::cout. Control period setted to 200 Hz because at 1 kHz communication errors occur.
* Modified some vector types according to new API methods.
* Fixed too high qbmove shaft limits

2.2.1 (2021-08-27)
------------------

2.2.0 (2021-08-26)
------------------
* Removed debug cout from code
* Added limits settings in qbmove. Fixed some minor bugs
* Fixed stiffness misreading in qbmove with new firmware version.
* Added some images to exapli the software functionalities. Added waypoints. Fixed a bug.
* Fixed a bug(position factor in constructor was not considered). Basic claw plugin implemented and tested.
* Created a new plugin to control qb SoftClaw
* Update license

2.1.3 (2019-10-07)
------------------

2.1.2 (2019-06-11)
------------------
* Fix dependencies

2.1.1 (2019-06-11)
------------------
* Fix dependencies

2.1.0 (2019-05-28)
------------------
* Fix minor style issues
* Add an interactive marker to control the device
* Fix minor style issues

2.0.2 (2018-08-09)
------------------
* Update license agreement copyright

2.0.1 (2018-08-07)
------------------

2.0.0 (2018-05-30)
------------------
* Refactor transmission initialization
* Refactor initialization
* Prepare for CombinedRobotHW compatibility

1.0.6 (2017-11-24)
------------------
* Add forgotten assertion of the shaft encoder

1.0.5 (2017-06-27)
------------------
* Fix C++11 support for cmake version less than 3.1

1.0.4 (2017-06-23)
------------------
* Update cmake version to match Kinetic standards

1.0.3 (2017-06-22)
------------------
* fix dependencies

1.0.2 (2017-06-21)
------------------
* fix cmake settings to solve isolated builds

1.0.1 (2017-06-19)
------------------
* first public release for Kinetic
